export const updateForecast = (value) => {
  return {
    type: 'UPDATE_FORECAST',
    value
  }

};
export const updateCity = (value) => {
  return {
    type: 'UPDATE_CITY',
    value
  }
};

export const toggleLoader = (status) => {
  return {
    type: 'TOGGLE_LOADER',
    status
  }
};

export const fetchForecast = (city) => {
  return async (dispatch) => {
    dispatch(toggleLoader(true));
    const accessKey = '2cb73e3265b714cd22dcf86776827596';
    const response = await fetch("http://api.weatherstack.com/current?access_key=" + accessKey + "&query=" + city)
    const data = await response.json();
    console.log(data);
    dispatch(updateForecast(data));
    dispatch(toggleLoader(false))
  }
};
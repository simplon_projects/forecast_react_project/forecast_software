import React, { Component } from 'react';

class ForecastTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date().toLocaleDateString('fr-fr', { weekday: 'short', day: 'numeric', month: 'short' }),
        }
    }
    render() {
        return (
            <div>
                <h1
                    style={{
                        fontFamily: 'Montserrat',
                        fontSize: '62px',
                        color: 'white',
                        textAlign:'center',
                        margin:'25px'
                    }}
                >{this.props.city}</h1>
            
                <p
                    style={{
                        fontFamily: 'Montserrat',
                        fontSize: '22px',
                        color: 'white',
                        textAlign:'center'
                    }}>{this.state.date}</p>
            </div>

        )
    }
}
export default ForecastTitle;
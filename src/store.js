import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import forecast from "./reducers/forecast";

const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});
const middleware = [
    thunk
];
let store = createStore(combineReducers({ forecast }),
    composeEnhancers(
        applyMiddleware(...middleware)
    ),
);

export default store;
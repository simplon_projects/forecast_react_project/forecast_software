import React, { Component } from 'react';
import Forecast from './components/Forecast';

class App extends Component {
  render() {
    return (
    <div className="App">
      <header className="App-header">
        <div>
          <Forecast />
        </div>
      </header>
    </div>
  );
  }
  
}

export default App;

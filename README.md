# Simplon Project #CDA

## Project Overview :

Exercice d'apprentissage Electron 



## ScreenShot :

![./src/assets/ScreenShot_Forecast-software.png](./src/assets/ScreenShot_Forecast-software.png)





## How To Run :

Open project folder in a terminal and run following scripts :

```shell
yarn package
```

```shell
yarn start
```



## Consignes

- ### ⛵️ Installer Yarn

  [Installer et utiliser Yarn sous Linux Debian – Blog Net-Expression](https://blog.net-expression.com/administration/installation/installer-utiliser-yarn-linux/)

  ```
  $ sudo apt install curl
  $ curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  $ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  $ sudo apt update
  $ sudo apt install yarn
  ```

  ### ⛵️Créer un nouveau projet boilerplate React Electron

  [electron-react-boilerplate/electron-react-boilerplate](https://github.com/electron-react-boilerplate/electron-react-boilerplate)

  ```
  git clone --depth 1 --single-branch https://github.com/electron-react-boilerplate/electron-react-boilerplate.git forecast-software
  cd forecast-software
  yarn
  ```

  ### ⛵️Modifier les fichiers de configs

  - .erb/configs/webpack.config.base.js
    - Ligne 15 : `test: /\.[jt]sx?$/,`
  - .erb/configs/webpack.config.renderer.dev.babel.js
    - Ligne 47 : `require.resolve('../../src/index.js'),`
  - .erb/configs/webpack.config.renderer.prod.babel.js
    - Ligne 33 : `path.join(__dirname, '../../src/index.js'),`  

  ### ⛵️ Ajouter les dépendances

  - redux
  - react-redux
  - redux-thunk

  ### ⛵️ Modification du répertoire src

  - Modifier le index.tsx et index.js
  - Copier coller les répertoires et fichiers suivant dans le dossier src/
    - actions/
    - components/
    - reducers/
    - services/
    - store.js
  - Modifier le index.js du projet electron, avec le votre

  ### 